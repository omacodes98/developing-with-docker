# Developing with Docker 
- Use Docker for local development
- Using Docker Compose to run multiple Docker containers 

## Table of Contents
- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps with Docker](#steps-with-docker)

- [Steps with Docker Compose](#steps-with-docker-compose)

- [Installation](#installation)

- [Screenshots](#screenshots)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Created Dockerfile for Nodejs application and build Docker image 

* Ran Nodejs application in Docker container and connected to MongoDB database container locally

* Ran MongoExpress container as a UI of the MongoDB database

## Technology Used

* Docker 

* Mongodb

* Mongo Express


### Steps with Docker 

Step 1: Create docker network

    docker network create mongo-network 

Step 2: start mongodb 

    docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --name mongodb --net mongo-network mongo    

Step 3: start mongo-express
    
    docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express   

_NOTE: creating docker-network in optional. You can start both containers in a default network. In this case, just emit `--net` flag in `docker run` command_

Step 4: open mongo-express from browser

    http://localhost:8081

Step 5: create `user-account` _db_ and `users` _collection_ in mongo-express

Step 6: Start your nodejs application locally - go to `app` directory of project 

    cd app
    npm install 
    node server.js
    
Step 7: Access you nodejs application UI from browser

    http://localhost:3000


## Steps with Docker Compose 

Step 1: start mongodb and mongo-express

    docker-compose -f docker-compose.yaml up
    
_You can access the mongo-express under localhost:8080 from your browser_
    
Step 2: in mongo-express UI - create a new database "my-db"

Step 3: in mongo-express UI - create a new collection "users" in the database "my-db"       
    
Step 4: start node server 

    cd app
    npm install
    node server.js
    
Step 5: access the nodejs application from browser 

    http://localhost:3000

#### To build a docker image from the application

    docker build -t my-app:1.0 .       
    
The dot "." at the end of the command denotes location of the Dockerfile.


## Installation

Run $ npm install to get all dependecies for application 

## Screenshots 

![Docker Hub](/images/01_docker_hub_mongodb_images.png)
> Mongodb image on Docker Hub 

![Docker Hub 2](/images/02_docker_hub_mongo_express_image.png)
> Mongo-Express image on Docker Hub 

![Mongo Network](/images/03_creating_mongo_network_on_docker.png)
> Mongo Network Creation on Docker 

![Mongodb Container](/images/04_creating_mongodb_container.png)
> Creating Mongodb Container from mongo image 

![Mongo-Express Container](/images/05_creating_mongo_express_container.png)
> Creating Monge-Express Container with Mongo-Express image

![Monngo Express UI](/images/06_mongo_express_ui.png)
> Mongo-Express UI 

![User Account DB](/images/07_user_account_db_created.png)
> User Account DB created 

![Collection Creation](/images/08_creating_collection_in_user_db.png)
> Creating Collections 

![Running Application](/images/09_running_application_on_server.png)
> Running Application on Server 

![Application UI](/images/10_application_UI.png)
> Application UI 

![Docker Compose](/images/11_starting_container_with_docker_compose.png)
> Running Multiple Containers with Docker Compose 

![Building App image](/images/12_building_app_image_with_dockerfile.png)
> Building app image with Dockerfile

![Running App Container](/images/13_running_app_container.png)
> Running app Container 

[App Container Terminal](/images/14_app_container_interactive_terminal.png)
> App Container Interactive Terminal

## Usage 

Run $ node server.js 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/developing-with-docker.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/developing-with-docker

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.